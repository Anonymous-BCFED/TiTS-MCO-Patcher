@echo off
set MCO_DIR=%CD%

set FLEXSDK_VERSION=4.16.1
set FLEXSDK_URL=http://apache.osuosl.org/flex/4.16.1/binaries/apache-flex-sdk-4.16.1-bin.zip

::echo Administrative permissions required. Detecting permissions...
::net session >nul 2>&1
::if %errorLevel% == 0 (
::    echo Success: Administrative permissions confirmed.
::) else (
::    echo Failure: Current permissions inadequate.
::    echo You must start .\bootstrap\windows\10.bat with administrative privileges!
::    exit /B 1
::)

echo Using Chocolatey to install dependencies.
echo This will call a VBS script for admin privileges, so you will be asked if that's OK by UAC and probably antivirus.
call lib/sudo choco install -y ant jdk11 git git-lfs python3 flashdevelop

:: Ensure hooks are set up properly.
git lfs install

:: Apache Flex SDK
::echo Installing Apache Flex SDK.
::IF NOT EXIST %TEMP%\flexsdk.zip (
::    wget -O %TEMP%\flexsdk.zip %FLEXSDK_URL%
::)
::
::IF NOT EXIST %MCO_DIR%\lib\apache-flex-sdk-%FLEXSDK_VERSION%-bin (
::    mkdir %MCO_DIR%\lib\apache-flex-sdk-%FLEXSDK_VERSION%-bin
::    cd %MCO_DIR%\lib\apache-flex-sdk-%FLEXSDK_VERSION%-bin
::    7z x %TEMP%\flexsdk.zip
::    cd %MCO_DIR%
::)
::cd %MCO_DIR%\lib\apache-flex-sdk-%FLEXSDK_VERSION%-bin
:: Bootstrap completed in mco/__main__.py
::ant frameworks-rsls
::ant -f installer.xml
::cd %MCO_DIR%

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Submodules
git submodule update --init --recursive

:: Python packages. Pray we don't need wheels.
pip install -r requirements.txt

MCO_DIR=`pwd`

FLEXSDK_VERSION="4.16.1"
FLEXSDK_MD5="0fba6c912c3919ae1b978ca2d053fe07"
FLEXSDK_URL="http://apache.osuosl.org/flex/${FLEXSDK_VERSION}/binaries/apache-flex-sdk-${FLEXSDK_VERSION}-bin.tar.gz"
FLEXSDK_DIR="${MCO_DIR}/lib/apache-flex-sdk-${FLEXSDK_VERSION}-bin"

AIRSDK_URL="http://airdownload.adobe.com/air/lin/download/2.6/AdobeAIRSDK.tbz2"

FLASHPLAYERDEBUG_URL="https://fpdownload.macromedia.com/pub/flashplayer/updaters/32/flash_player_sa_linux_debug.x86_64.tar.gz"

PLAYERGLOBAL_URL="https://fpdownload.macromedia.com/get/flashplayer/updaters/32/playerglobal32_0.swc"
PLAYERGLOBAL_VERSION="32.0"
export PLAYERGLOBAL_HOME=$MCO_DIR/lib/playerglobal

# Helpers
function CHECKHASH() {
  echo "$3 *$2" | $1 -c
  if [ $? != 0 ]; then
    rm -f $2
    echo "$2 has an incorrect checksum! Exiting."
    exit 1
  fi
}

function CHECKMD5() {    CHECKHASH md5sum $1 $2; }
function CHECKSHA512() { CHECKHASH sha512sum $1 $2; }

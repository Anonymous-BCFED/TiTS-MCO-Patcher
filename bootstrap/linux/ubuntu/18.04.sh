#!/bin/bash
MCO_DIR=`pwd`
source $MCO_DIR/bootstrap/linux/_common.sh

sudo apt install -y software-properties-common
# git-lfs
sudo curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
sudo apt install -y ant openjdk-11-jdk openjdk-11-jre git git-lfs python3-venv

git lfs install

################################################################################
# TiTS
# We have to clone this seperately, due to licensing.
#if [ ! -d $MCO_DIR/lib/TiTS-Public ]; then
#  echo "##########################################################################"
#  echo "# Cloning TiTS.  This will take a while, especially on slow connections. #"
#  echo "##########################################################################"
#  git clone https://github.com/OXOIndustries/TiTS-Public.git $MCO_DIR/lib/TiTS-Public
#fi

################################################################################
# Adobe AIR SDK
#if [ ! -f /tmp/airsdk.tbz2 ]; then
#  wget -O /tmp/airsdk.tbz2 $AIRSDK_URL
#fi
#AIR_HOME=$MCO_DIR/lib/AdobeAIRSDK
#mkdir -p $AIR_HOME
#cd $AIR_HOME
#tar xjf /tmp/airsdk.tbz2
#cd $MCO_DIR

################################################################################
# Adobe Flash Player Debugger
#if [ ! -f /tmp/flashplayer.tgz ]; then
#  wget -O /tmp/flashplayer.tgz $FLASHPLAYERDEBUG_URL
#fi
#FLASHPLAYER_DEBUGGER_DIR=$MCO_DIR/lib/flashplayer
#mkdir -p $FLASHPLAYER_DEBUGGER_DIR
#cd $FLASHPLAYER_DEBUGGER_DIR
#tar xzf /tmp/flashplayer.tgz
#FLASHPLAYER_DEBUGGER=$FLASHPLAYER_DEBUGGER_DIR/flashplayerdebugger
#cd $MCO_DIR

################################################################################
# Adobe Flash Player playerglobal files.
#if [ ! -f $PLAYERGLOBAL_HOME/$PLAYERGLOBAL_VERSION/playerglobal.swc ]; then
#  mkdir -p $PLAYERGLOBAL_HOME
#  wget -O $PLAYERGLOBAL_HOME/$PLAYERGLOBAL_VERSION/playerglobal.swc $PLAYERGLOBAL_URL
#fi

################################################################################
# Apache Flex SDK
if [ ! -f /tmp/flexsdk.tgz ]; then
  wget -O /tmp/flexsdk.tgz $FLEXSDK_URL
fi

CHECKMD5 /tmp/flexsdk.tgz $FLEXSDK_MD5

mkdir -p $MCO_DIR/lib
cd $MCO_DIR/lib
tar xzf /tmp/flexsdk.tgz
cd apache-flex-sdk-$FLEXSDK_VERSION-bin
#ant framework-rsls
#ant -f installer.xml
cd $MCO_DIR

###############################################################################
# Submodules
git submodule update --init --recursive

# Python3.6 is installed by default, so no need to do anything here.
python3.6 -m venv --clear .venv
source ./.venv/bin/activate
pip3.6 install -r requirements.txt

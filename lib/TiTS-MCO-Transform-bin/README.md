# TiTS:MCO Abstract Syntax Tree Transforms
These files are compiled binaries from the [TiTS: MCO AST Transformation Toolkit](https://gitlab.com/Anonymous-BCFED/TiTS-MCO-Transform). Compiling Flex with the fixes we use is a major undertaking due to upstream buildsystem bugs and loads of dependencies, so it is included here as a binary.

# Components
| File(s) | License | Notes |
|---|---|---|
| `lib/asanalyze-1.0-SNAPSHOT.jar` | [MIT Open-Source Software](https://gitlab.com/VentureGuy/asanalyze/blob/master/LICENSE) | [AST analysis toolkit](https://gitlab.com/VentureGuy/asanalyze) from VentureGuy
| `lib/{asc,mxmlc,swfutils}-*.jar` | [Apache License 2.0](https://gitlab.com/VentureGuy/flex-sdk/blob/mco-patches/LICENSE) | [Modified build of Apache Flex](https://gitlab.com/VentureGuy/flex-sdk)

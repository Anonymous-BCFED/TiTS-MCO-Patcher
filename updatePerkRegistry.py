import codecs
import os
import shutil

import yaml
import chardet
import ftfy
from buildtools import log, os_utils
from as3.fileops import detect_encoding
from unfuckEncoding import unfuckFile

BASEDIR = 'classes/GameData/PerkClasses'


def getCurrentIndent(line):
    indent = ''
    for c in line:
        if c not in (' ', '\t'):
            return indent
        indent += c


def cleanName(name):
    newName = ''
    for c in name:
        c = c.upper()
        if c in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ ':
            newName += c
    return newName.replace(' ', '_')

def main():
    log.info("UPDATEPERKREGISTRY 1.0")

    classnames = []
    classname2Realname = {}
    ''' RIP OLD AND BUSTED
    for filename in os.listdir(BASEDIR):
        if not filename.endswith('.as'):
            continue
        classname = filename[:-3]
        filename = os.path.join(BASEDIR, filename)
        if os.path.isfile(filename):
            # print classname
            classnames += [classname]
            encoding = detect_encoding(filename)
            with log.info('Reading %s (%s)...', filename, encoding):
                with codecs.open(filename, 'r', encoding=encoding) as f:
                    for line in f:
                        s_line = line.strip()
                        if s_line.startswith('perkName = "'):
                            perkName = s_line[12:-2]
                            classname2Realname[classname] = perkName
                            log.info('Perk Name: %s', perkName)
    '''
    with open('parsed_perks.yml' , 'r') as f:
        data = yaml.load(f)
        for perk in data.values():
            className = perk['className']
            perkName = perk['perkName']
            classnames += [className]
            classname2Realname[className]=perkName
            log.info('Perk Name: %s', perkName)
    with codecs.open('ALLPERKS.yml', 'w', encoding='utf-8-sig') as f:
        yaml.dump({'Perks': classname2Realname}, f)
    seclassnames=[]
    seclassname2Realname={}
    with open('parsed_status_effects.yml' , 'r') as f:
        data = yaml.load(f)
        for se in data.values():
            className = se['className']
            statusEffectName = se['statusName']
            seclassnames += [className]
            seclassname2Realname[className]=statusEffectName
            log.info('Status Effect Name: %s', statusEffectName)
    with codecs.open('ALLSTATUSEFFECTS.yml', 'w', encoding='utf-8-sig') as f:
        yaml.dump({'StatusEffects': seclassname2Realname}, f)
    with codecs.open('classes/GameData/PerkRegistry.as', 'r', encoding='utf-8-sig') as inf:
        with codecs.open('classes/GameData/PerkRegistry.as.tmp', 'w', encoding='utf-8-sig') as outf:
            in_autoblock = False
            for line in inf:
                line = line.rstrip()
                if line.endswith('/// BEGIN PERK STATICS'):
                    log.info('Entered perk statics')
                    in_autoblock = True
                    indent = getCurrentIndent(line)
                    outf.write(line + '\n')
                    for perk in classnames:
                        outf.write('{INDENT}public static var {NAME}:{CLASSNAME} = new {CLASSNAME}();\n'.format(INDENT=indent, NAME=cleanName(classname2Realname[perk]), CLASSNAME=perk))
                    log.info('Wrote %d', len(classnames))
                if line.endswith('/// BEGIN PERK INSTANTIATION'):
                    log.info('Entered perk instances')
                    in_autoblock = True
                    indent = getCurrentIndent(line)
                    outf.write(line + '\n')
                    for perk in classnames:
                        outf.write('{INDENT}_all["{NAME}"] = {CLASSNAME};\n'.format(INDENT=indent, NAME=classname2Realname[perk], CLASSNAME=cleanName(classname2Realname[perk])))
                    log.info('Wrote %d', len(classnames))
                if line.endswith('/// END PERK STATICS'):
                    in_autoblock = False
                    log.info('Left perk statics')
                if line.endswith('/// END PERK INSTANTIATION'):
                    in_autoblock = False
                    log.info('Left perk instances')
                if in_autoblock:
                    continue
                outf.write(line + '\n')
    shutil.move('classes/GameData/PerkRegistry.as.tmp', 'classes/GameData/PerkRegistry.as')
    unfuckFile('classes/GameData/PerkRegistry.as')

if __name__ == '__main__':
    main()

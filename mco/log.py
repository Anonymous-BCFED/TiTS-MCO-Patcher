import logging, os, sys, platform
from typing import Union, Type
from mco.constants import IS_LINUX, IS_WINDOWS

BOLD = '\x1b[1m'
UNBOLD = '\x1b[0m'

def defaultSetup(level=logging.INFO, fmt='%(message)s', display_prefix=True):
    logger = logging.getLogger()
    logger.setLevel(level)
    sh = logging.StreamHandler()
    formatter = logging.Formatter(fmt=fmt, datefmt='%m/%d/%Y %I:%M:%S %p')
    sh.setFormatter(formatter)
    istty = sys.stdout.isatty() and not IS_WINDOWS
    def decorate_emit(fn):
        # add methods we need to the class
        def new(*args):
            levelno = args[0].levelno
            prefix = ''
            if(levelno >= logging.CRITICAL):
                color = '\x1b[31;1m' if istty else ''
                prefix = 'CRITICAL: '
            elif(levelno >= logging.ERROR):
                color = '\x1b[31;1m' if istty else ''
                prefix = 'ERROR: '
            elif(levelno >= logging.WARNING):
                color = '\x1b[33;1m' if istty else ''
                prefix = 'WARNING: '
            elif(levelno >= logging.INFO):
                color = '\x1b[0m' if istty else ''
            elif(levelno >= logging.DEBUG):
                color = '\x1b[35;1m' if istty else ''
                prefix = 'DEBUG: '
            else:
                color = '\x1b[0m' if istty else ''
            clear = '\x1b[0m' if istty else ''
            if not display_prefix:
                prefix = ''

            indent = LogWrapper.INDENT * '  '
            args[0].msg = f"{indent}{color}{prefix}{args[0].msg}{clear}"

            # new feature i like: bolder each args of message
            #args[0].args = tuple('\x1b[1m' + arg + '\x1b[0m' for arg in args[0].args)
            # Broken
            return fn(*args)
        return new
    sh.emit = decorate_emit(sh.emit)
    logger.addHandler(sh)

def verboseSetup(level=logging.INFO):
    defaultSetup(level, '%(asctime)s [%(levelname)-8s]: %(message)s', False)

class LogWrapper(object):
    INDENT = 0

    def __init__(self, logger=None):
        self.log = logger
        if isinstance(self.log, str):
            self.log = logging.getLogger(self.log)
        if self.log is None:
            self.log = logging.getLogger()

    def __enter__(self):
        self.indent()
        return self

    def __exit__(self, type, value, traceback):
        self.dedent()
        return False

    def indent(self) -> None:
        LogWrapper.INDENT += 1

    def dedent(self) -> None:
        LogWrapper.INDENT -= 1

    def debug(self, msg, *args, **kwargs):
        """
        Log 'msg % args' with severity 'DEBUG'.

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.

        logger.debug("Houston, we have a %s", "thorny problem", exc_info=1)
        """
        if self.log.isEnabledFor(logging.DEBUG):
            self._log(logging.DEBUG, msg, args, **kwargs)
        return self

    def info(self, msg, *args, **kwargs):
        """
        Log 'msg % args' with severity 'INFO'.

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.

        logger.info("Houston, we have a %s", "interesting problem", exc_info=1)
        """
        if self.log.isEnabledFor(logging.INFO):
            self._log(logging.INFO, msg, args, **kwargs)
        return self

    def warning(self, msg, *args, **kwargs):
        """
        Log 'msg % args' with severity 'WARNING'.

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.

        logger.warning("Houston, we have a %s", "bit of a problem", exc_info=1)
        """
        if self.log.isEnabledFor(logging.WARNING):
            self._log(logging.WARNING, msg, args, **kwargs)
        return self

    warn = warning

    def error(self, msg, *args, **kwargs):
        """
        Log 'msg % args' with severity 'ERROR'.

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.

        logger.error("Houston, we have a %s", "major problem", exc_info=1)
        """
        if self.log.isEnabledFor(logging.ERROR):
            self._log(logging.ERROR, msg, args, **kwargs)
        return self

    def exception(self, msg, *args, **kwargs):
        """
        Convenience method for logging an ERROR with exception information.
        """
        kwargs['exc_info'] = 1
        self.error(msg, *args, **kwargs)
        return self

    def critical(self, msg, *args, **kwargs):
        """
        Log 'msg % args' with severity 'CRITICAL'.

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.

        logger.critical("Houston, we have a %s", "major disaster", exc_info=1)
        """
        if self.log.isEnabledFor(logging.CRITICAL):
            self._log(logging.CRITICAL, msg, args, **kwargs)
        return self

    def arbitrary(self, level: int, msg: str, *args, **kwargs):
        if self.log.isEnabledFor(level):
            self._log(level, msg, args, **kwargs)
        return self

    def _log(self, level, msg, args, exc_info=None, extra=None):
        self.log._log(level, msg, args, exc_info, extra)



def getLogger(name: Union[str, Type]) -> LogWrapper:
    return LogWrapper(logging.getLogger(name))

import pygit2, tqdm

class MCOProgressCallbacks(pygit2.RemoteCallbacks):
    def __init__(self, credentials=None, certificate=None):
        super().__init__(credentials=credentials, certificate=certificate)
        self.tqdmObjDownload = None
        self.tqdmObjIndex = None
        self.tqdmDeltaIndex = None
        self.lastStage = ''

    def startup(self):
        self.tqdmObjDownload = tqdm.tqdm(ascii=True, desc="Receiving objects")
        self.tqdmObjIndex = tqdm.tqdm(ascii=True, desc="Indexing objects")
        self.tqdmDeltaIndex = tqdm.tqdm(ascii=True, desc="Receiving deltas", unit='delta')
    def shutdown(self):
        self.tqdmDeltaIndex.close()
        self.tqdmObjIndex.close()
        self.tqdmObjDownload.close()

    def _setup_tqdm(self, _tqdm, message, current, total, unit):
        _tqdm.desc = message
        _tqdm.total = _tqdm.last_print_t = total
        _tqdm.n = _tqdm.last_print_n = current
        _tqdm.unit = unit

    def transfer_progress(self, stats):
        #self.tqdm.clear()
        if stats.total_objects > 0:
            if self.tqdmObjDownload.total != stats.total_objects:
                self._setup_tqdm(self.tqdmObjDownload, 'Receiving objects', stats.received_objects, stats.total_objects, 'obj')
                self._setup_tqdm(self.tqdmObjIndex, 'Indexing objects', stats.indexed_objects, stats.total_objects, 'obj')
            self.tqdmObjDownload.n = stats.received_objects
            self.tqdmObjIndex.n = stats.indexed_objects
        if stats.total_deltas > 0:
            if self.tqdmDeltaIndex.total != stats.total_deltas:
                self._setup_tqdm(self.tqdmDeltaIndex, 'Indexing deltas', stats.indexed_deltas, stats.total_deltas, 'delta')
            self.tqdmDeltaIndex.n = stats.indexed_deltas
        #self.tqdmDeltaIndex.set_postfix({
        #    'rO': stats.received_objects,
        #    'iO': stats.indexed_objects,
        #    'tO': stats.total_objects,
        #    'iD': stats.indexed_deltas,
        #    'tD': stats.total_deltas,
        #    'recv bytes': stats.received_bytes,
        #}, refresh=False)
        self.tqdmObjDownload.set_postfix_str(f'recv: {stats.received_bytes}B', refresh=False)
        self.tqdmDeltaIndex.update()
        self.tqdmObjIndex.update()
        self.tqdmObjDownload.update()

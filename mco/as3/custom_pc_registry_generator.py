import os, yaml
from mco.constants import TEMPLATES_DIR, MCO_DIR
from mco.encoding import unfuckFile
from mco.log_utils import log
from mco.as3.fileops import getIndentChars, scanForFilesIn
from mco.game.status_effect import StatusEffect
from mco.game.perk import Perk
from mco.game.custom_pc import CustomPC
from mco.mods.modlist import ModList
from typing import List, Any
import string
import shutil
import subprocess

def loadDefinedCustomPCs(config: dict, mods: ModList) -> dict:
    o = {}
    filesToScan=[]
    filesToScan += scanForFilesIn(os.path.join(MCO_DIR, 'lib', 'TiTS-MCO-Patches', 'data', 'custom_pcs'), ext='.yml')
    for mod in mods.AllMods:
        modCPCPath = os.path.join(mod.path, 'data', 'custom_pcs')
        if os.path.isdir(modCPCPath):
            filesToScan += scanForFilesIn(modCPCPath, ext='.yml')
    for filename in filesToScan:
        o.update(loadCustomPCsFrom(filename))
    return o

def loadCustomPCsFrom(filename: str) -> dict:
    o={}
    with open(filename, 'r') as f:
        data = yaml.load(f, Loader=yaml.BaseLoader)
        for perkID, perkData in data.items():
            o[perkID]=perkData
    return o

def genCustomPCRegistry(customPCs: List[CustomPC], outfile: str) -> None:
    customPCs.sort(key=lambda x: x.ID)
    with open(os.path.join(TEMPLATES_DIR, 'CustomPCRegistry.tmp.as'), 'r', encoding='utf-8-sig') as inf:
        with open(outfile + '.tmp', 'w', encoding='utf-8-sig') as outf:
            in_autoblock = False
            for line in inf:
                line = line.rstrip()
                if line.endswith('/// BEGIN CUSTOM PC STATICS'):
                    log.info('Entered custom PC statics')
                    in_autoblock = True
                    indent = getIndentChars(line)
                    outf.write(line + '\n')
                    for customPC in customPCs:
                        outf.write('{INDENT}public static var {CONSTNAME}:{CLASSNAME} = new {CLASSNAME}();\n'.format(INDENT=indent, CONSTNAME=customPC.getConstName(), CLASSNAME=customPC.getClassName()))
                    log.info('Wrote %d', len(customPCs))
                if line.endswith('/// BEGIN CUSTOM PC INSTANTIATION'):
                    log.info('Entered custom instances')
                    in_autoblock = True
                    indent = getIndentChars(line)
                    outf.write(line + '\n')
                    for customPC in customPCs:
                        outf.write('{INDENT}_all["{NAME}"] = {CLASSNAME};\n'.format(INDENT=indent, NAME=customPC.ID, CONSTNAME=customPC.getConstName(), CLASSNAME=customPC.getClassName()))
                        for customPCref in list(set([customPC.ID]+customPC.AllNames)):
                            outf.write('{INDENT}//_serID2ID["{NAME}"] = "{ID}";\n'.format(INDENT=indent, NAME=customPC.Name, ID=customPC.ID, CONSTNAME=customPC.getConstName(), CLASSNAME=customPC.getClassName(), ))
                    log.info('Wrote %d', len(customPCs))
                    outf.write(f'{indent}// Wrote {len(customPCs)}.\n')
                if line.endswith('/// END CUSTOM PC STATICS'):
                    in_autoblock = False
                    log.info('Left custom PC statics')
                if line.endswith('/// END CUSTOM PC INSTANTIATION'):
                    in_autoblock = False
                    log.info('Left custom PC instances')
                if in_autoblock:
                    continue
                outf.write(line + '\n')
    shutil.move(outfile + '.tmp', outfile)
    unfuckFile(outfile)

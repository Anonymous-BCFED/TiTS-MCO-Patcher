import os, yaml
from mco.constants import TEMPLATES_DIR, MCO_DIR
from mco.encoding import unfuckFile
from mco.log_utils import log
from mco.as3.fileops import getIndentChars, scanForFilesIn
from mco.game.status_effect import StatusEffect
from mco.game.perk import Perk
from mco.game.custom_pc import CustomPC
from mco.mods.modlist import ModList
from typing import List, Any
import string
import shutil
import subprocess


def cleanName(name: str) -> str:
    newName = ''
    for c in name:
        c = c.upper()
        if c in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ ':
            newName += c
    return newName.replace(' ', '_')

def loadDefinedPerks(config: dict, mods: ModList) -> dict:
    o = {}
    filesToScan=[]
    filesToScan += scanForFilesIn(os.path.join(MCO_DIR, 'lib', 'TiTS-MCO-Patches', 'data', 'perks'), ext='.yml')
    for mod in mods.AllMods:
        modPerkPath = os.path.join(mod.path, 'data', 'perks')
        if os.path.isdir(modPerkPath):
            filesToScan += scanForFilesIn(modPerkPath, ext='.yml')
    for filename in filesToScan:
        o.update(loadPerksFrom(filename))
    return o

def loadPerksFrom(filename: str) -> dict:
    o={}
    with open(filename, 'r') as f:
        data = yaml.load(f, Loader=yaml.BaseLoader)
        for perkID, perkData in data.items():
            o[perkID]=perkData
    return o

def genPerkRegistry(perks: List[Perk], outfile: str) -> None:
    perks.sort(key=lambda x: x.ID)
    with open(os.path.join(TEMPLATES_DIR, 'PerkRegistry.tmp.as'), 'r', encoding='utf-8-sig') as inf:
        with open(outfile + '.tmp', 'w', encoding='utf-8-sig') as outf:
            in_autoblock = False
            for line in inf:
                line = line.rstrip()
                if line.endswith('/// BEGIN PERK STATICS'):
                    log.info('Entered perk statics')
                    in_autoblock = True
                    indent = getIndentChars(line)
                    outf.write(line + '\n')
                    for perk in perks:
                        outf.write('{INDENT}public static var {CONSTNAME}:{CLASSNAME} = new {CLASSNAME}();\n'.format(INDENT=indent, CONSTNAME=perk.getConstName(), CLASSNAME=perk.getClassName()))
                    log.info('Wrote %d', len(perks))
                if line.endswith('/// BEGIN PERK INSTANTIATION'):
                    log.info('Entered perk instances')
                    in_autoblock = True
                    indent = getIndentChars(line)
                    outf.write(line + '\n')
                    for perk in perks:
                        outf.write('{INDENT}_all["{NAME}"] = {CLASSNAME};\n'.format(INDENT=indent, NAME=perk.ID, CONSTNAME=perk.getConstName(), CLASSNAME=perk.getClassName()))
                        for perkref in list(set([perk.ID]+perk.AllNames)):
                            outf.write('{INDENT}//_serID2ID["{NAME}"] = "{ID}";\n'.format(INDENT=indent, NAME=perk.Name, ID=perk.ID, CONSTNAME=perk.getConstName(), CLASSNAME=perk.getClassName(), ))
                    log.info('Wrote %d', len(perks))
                if line.endswith('/// END PERK STATICS'):
                    in_autoblock = False
                    log.info('Left perk statics')
                if line.endswith('/// END PERK INSTANTIATION'):
                    in_autoblock = False
                    log.info('Left perk instances')
                if in_autoblock:
                    continue
                outf.write(line + '\n')
    shutil.move(outfile + '.tmp', outfile)
    unfuckFile(outfile)

def genCustomPCRegistry(customPCs: List[CustomPC], outfile: str) -> None:
    customPCs.sort(key=lambda x: x.ID)
    with open(os.path.join(TEMPLATES_DIR, 'CustomPCRegistry.tmp.as'), 'r', encoding='utf-8-sig') as inf:
        with open(outfile + '.tmp', 'w', encoding='utf-8-sig') as outf:
            in_autoblock = False
            for line in inf:
                line = line.rstrip()
                if line.endswith('/// BEGIN CUSTOM PC STATICS'):
                    log.info('Entered custom PC statics')
                    in_autoblock = True
                    indent = getIndentChars(line)
                    outf.write(line + '\n')
                    for customPC in customPCs:
                        outf.write('{INDENT}public static var {CONSTNAME}:{CLASSNAME} = new {CLASSNAME}();\n'.format(INDENT=indent, CONSTNAME=customPC.getConstName(), CLASSNAME=customPC.getClassName()))
                    log.info('Wrote %d', len(customPCs))
                if line.endswith('/// BEGIN CUSTOM PC INSTANTIATION'):
                    log.info('Entered custom instances')
                    in_autoblock = True
                    indent = getIndentChars(line)
                    outf.write(line + '\n')
                    for customPC in customPCs:
                        outf.write('{INDENT}_all["{NAME}"] = {CLASSNAME};\n'.format(INDENT=indent, NAME=customPC.ID, CONSTNAME=customPC.getConstName(), CLASSNAME=customPC.getClassName()))
                        for customPCref in list(set([customPC.ID]+customPC.AllNames)):
                            outf.write('{INDENT}//_serID2ID["{NAME}"] = "{ID}";\n'.format(INDENT=indent, NAME=customPC.Name, ID=customPC.ID, CONSTNAME=customPC.getConstName(), CLASSNAME=customPC.getClassName(), ))
                    log.info('Wrote %d', len(customPCs))
                if line.endswith('/// END CUSTOM PC STATICS'):
                    in_autoblock = False
                    log.info('Left custom PC statics')
                if line.endswith('/// END CUSTOM PC INSTANTIATION'):
                    in_autoblock = False
                    log.info('Left custom PC instances')
                if in_autoblock:
                    continue
                outf.write(line + '\n')
    shutil.move(outfile + '.tmp', outfile)
    unfuckFile(outfile)

def genStatusEffectRegistry(statusEffects: List[StatusEffect], outfile: str) -> None:
    classname2Realname = {}
    classnames = {}
    for statusEffect in statusEffects:
        classname2Realname[statusEffect.ClassName] = statusEffect.Name
        classnames += [statusEffect.ClassName]
    classnames.sort()
    with open(os.path.join(TEMPLATES_DIR, 'StatusEffectRegistry.tmp.as'), 'r', encoding='utf-8-sig') as inf:
        with open(outfile + '.tmp', 'w', encoding='utf-8-sig') as outf:
            in_autoblock = False
            for line in inf:
                line = line.rstrip()
                if line.endswith('/// BEGIN STATUS STATICS'):
                    log.info('Entered status statics')
                    in_autoblock = True
                    indent = getIndentChars(line)
                    outf.write(line + '\n')
                    for statusEffect in classnames:
                        outf.write('{INDENT}public static var {NAME}:{CLASSNAME} = new {CLASSNAME}();\n'.format(INDENT=indent, NAME=cleanName(classname2Realname[statusEffect]), CLASSNAME=statusEffect))
                    log.info('Wrote %d', len(classnames))
                if line.endswith('/// BEGIN STATUS INSTANTIATION'):
                    log.info('Entered status instances')
                    in_autoblock = True
                    indent = getIndentChars(line)
                    outf.write(line + '\n')
                    for statusEffect in classnames:
                        outf.write('{INDENT}_all["{NAME}"] = {CLASSNAME};\n'.format(INDENT=indent, NAME=classname2Realname[statusEffect], CLASSNAME=cleanName(classname2Realname[statusEffect])))
                    log.info('Wrote %d', len(classnames))
                if line.endswith('/// END STATUS STATICS'):
                    in_autoblock = False
                    log.info('Left status statics')
                if line.endswith('/// END STATUS INSTANTIATION'):
                    in_autoblock = False
                    log.info('Left status instances')
                if in_autoblock:
                    continue
                outf.write(line + '\n')
    shutil.move(outfile + '.tmp', outfile)
    unfuckFile(outfile)

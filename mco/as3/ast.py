import os, yaml, subprocess
from mco.constants import TEMPLATES_DIR, MCO_DIR
from mco.log_utils import log

def parseAndPatch(modded_dir: str, knownPerks: dict) -> None:
    # We have to provide some data to Transform:
    # Name mappings
    DATA_PERK_MAPPING = os.path.join(modded_dir, 'data', 'mappings', 'perks.yml')
    # Pre-defined Perk data
    DATA_PERKS = os.path.join(modded_dir, 'data', 'perks.yml')

    INCLUDES_MAPFILE = os.path.join(modded_dir, 'data', 'includes.yml')

    if not os.path.isdir(os.path.dirname(DATA_PERK_MAPPING)):
        os.makedirs(os.path.dirname(DATA_PERK_MAPPING))

    with open(DATA_PERK_MAPPING, 'w') as f:
        yaml.dump({}, f, default_flow_style=False)

    with open(DATA_PERKS, 'w') as f:
        yaml.dump(knownPerks, f, default_flow_style=False)

    # LET THE GAMES BEGIN
    cmd=[
        'java',
        '-jar',
        os.path.join(MCO_DIR, 'lib', 'TiTS-MCO-Transform-bin', 'tits-mco-transform.jar'),
        '--includes', INCLUDES_MAPFILE,
        #'--trace',
        os.path.join(modded_dir, 'classes'),
        os.path.join(modded_dir, 'includes')
    ]
    print(' '.join([(repr(x) if ' ' in x else x) for x in cmd]))
    oldcwd = os.getcwd()
    os.chdir(modded_dir)
    subprocess.check_call(cmd)#, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    os.chdir(oldcwd)

def dumpAST(of_file, to_file, skip_processing=True):
    cmd=[
        'java',
        '-jar',
        os.path.join(MCO_DIR, 'lib', 'TiTS-MCO-Transform-bin', 'tits-mco-transform.jar'),
    ]
    if skip_processing:
        cmd += ['--skip-processing']
    cmd += [
        of_file,
        '--as-xml', to_file
    ]
    log.info('Calling: '+' '.join([(repr(x) if ' ' in x else x) for x in cmd]))
    subprocess.check_call(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

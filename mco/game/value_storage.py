class ValueStorageInfo(object):
    def __init__(self):
        self.ID=0
        self.Name=''
        self.Description=''

    def deserialize(self, _id, data):
        self.ID=_id
        if isinstance(data, str):
            self.Name=data
        else:
            self.Name=data['name']
            self.Description=data.get('description','')

    def serialize(self):
        return {
            'name': self.Name,
            'description': self.Description
        }

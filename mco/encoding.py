import os, codecs, ftfy, chardet, time, shutil, sys
from mco.log_utils import log
def detect_encoding(filename, detect_only=True):
    toread = min(32, os.path.getsize(filename))
    raw = b''
    with open(filename, 'rb') as f:
        raw = f.read(toread)
    encoding = 'utf-8-sig'
    if raw.startswith(codecs.BOM_UTF8):
        encoding = 'utf-8-sig'
    else:
        result = chardet.detect(raw)
        encoding = result['encoding']
        if encoding in ('utf-8', 'ascii'):
            encoding = 'utf-8-sig'
        if encoding in ('cp1252', 'Windows-1252'):
            encoding = 'cp1252'
    #log.info('chardet guesses: {}'.format(encoding))
    if encoding in ('utf-8-sig', 'cp1252') and not detect_only:
        with open(filename, 'r', encoding=encoding) as inf:
            with open(filename + '.utf8', 'w', encoding='utf-8-sig') as outf:
                for line in ftfy.fix_file(inf, fix_entities=False, fix_latin_ligatures=False, fix_character_width=False, uncurl_quotes=False):
                    outf.write(line)
        attempt=0
        while attempt<5:
            attempt += 1
            try:
                if os.path.isfile(filename):
                    os.remove(filename)
                if os.path.isfile(filename):
                    log.error('FAILED TO DELETE %s!', filename)
                    continue
                break
            except PermissionError:
                log.error("[%d/%d] Failed to delete %s, trying again in 1s.", attempt, 5, filename)
                time.sleep(1)
        shutil.move(filename + '.utf8', filename)
    return encoding

def unfuckFile(filepath: str, verbose: bool=True, encodings_detected: dict = None) -> None:
    '''
    Because TiTS is a mess of 50 different fucking encodings.
    '''
    guessed_encoding = detect_encoding(filepath, True)
    success = False
    indented = False
    for enctry in [guessed_encoding, 'utf-8-sig', 'utf-8', 'cp1252', 'Windows-1252']:
        try:
            if verbose:
                if not indented:
                    log.info('Unfucking %s (%s)...', filepath, enctry)
                else:
                    log.info('%s...?', enctry)
            if os.path.isfile(filepath+'.utf8'):
                os.remove(filepath+'.utf8')
            with codecs.open(filepath, 'r', enctry) as inf:
                with codecs.open(filepath + '.utf8', 'w', encoding='utf-8-sig') as outf:
                    for line in ftfy.fix_file(inf, encoding=enctry, fix_entities=False, fix_latin_ligatures=False, fix_character_width=False, uncurl_quotes=False):
                        outf.write(line)
            add_bom = False
            with open(filepath + '.utf8', 'rb') as f:
                add_bom = f.read(3) != codecs.BOM_UTF8
            if add_bom:
                with open(filepath + '.utf8b', 'wb') as outf:
                    outf.write(codecs.BOM_UTF8)
                    with open(filepath + '.utf8', 'rb') as inf:
                        outf.write(inf.read())
                if os.path.isfile(filepath+'.utf8'):
                    os.remove(filepath+'.utf8')
                shutil.move(filepath + '.utf8b', filepath + '.utf8')
            if os.path.isfile(filepath):
                os.remove(filepath)
            shutil.move(filepath + '.utf8', filepath)
            success = True
            if encodings_detected is not None:
                if enctry not in encodings_detected:
                    encodings_detected[enctry] = [filepath]
                else:
                    encodings_detected[enctry] += [filepath]
            break
        except UnicodeDecodeError:
            if verbose:
                if not indented:
                    log.INDENT += 1
                log.error('Couldn\'t decode using %s! Trying another...', enctry)
            indented = True
            continue
    if verbose and indented:
        log.INDENT -= 1
    if not success:
        log.error('Couldn\'t decode %s.', filepath)
        sys.exit(1)

import os, semantic_version, platform
#: Version of MCO.
MCO_VERSION=semantic_version.Version("0.0.1-alpha1")

#: MCO root dir.
MCO_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

#: Directory of template files.
TEMPLATES_DIR = os.path.join(MCO_DIR, 'templates')

#: Triggers an error if the patch format doesn't match up with the parser.
MCO_PATCH_FORMAT_VERSION = '201904221447'


SYSTEM = platform.system()
IS_WINDOWS = SYSTEM == 'Windows'
IS_LINUX = SYSTEM == 'Linux'

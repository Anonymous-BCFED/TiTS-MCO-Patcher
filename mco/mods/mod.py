import yaml, json, os, sys
import pygit2
from semantic_version import Version
from mco.game.perk import Perk

class Author(object):
    def __init__(self):
        self.Name = ''
        self.Email = ''
        self.Nickname = ''

    def deserialize(self, data):
        self.Name = data.get('name')
        self.Email = data.get('email')
        self.Nickname = data.get('nickname')

class VersionTest(object):
    def __init__(self, name, op, version):
        self.Name = name
        self.Operation = op
        self.Version = Version(version)

    def CheckAgainstMods(self, modlist, loadedMods=None):
        for mod in modlist.AllMods:
            if mod.Name == self.Name and (loadedMods is None or self.Name in loadedMods):
                if self.Operation is None:
                    return True
                if self.Operation == '==':
                    return self.Version == mod.Version
                elif self.Operation == '<=':
                    return self.Version <= mod.Version
                elif self.Operation == '>=':
                    return self.Version >= mod.Version
                elif self.Operation == '>':
                    return self.Version > mod.Version
                elif self.Operation == '<':
                    return self.Version < mod.Version
                elif self.Operation == '!=':
                    return self.Version != mod.Version
                else:
                    print(f'{self.Name} {self.Operation} {self.Version} - Invalid operation!')
                    sys.exit(1)
        return False




class Mod(object):
    def __init__(self):
        self.ID = ''
        self.Name = ''
        self.Repository = ''
        self.RepoLink = ''
        self.Authors = []
        self.License = ''
        self.Requires = []
        self.LoadAfter = []
        self.Conflicts = []
        self.Patches = []
        self.Includes = {}

        self.path = ''

    def LoadFrom(self, path):
        infofile = os.path.join(path, 'info.json')
        if os.path.isfile(infofile):
            with open(infofile, 'r') as f:
                self.path = path
                self.deserialize(json.load(f))
                return
        infofile = os.path.join(path, 'info.yml')
        if os.path.isfile(infofile):
            with open(infofile, 'r') as f:
                self.path = path
                self.deserialize(yaml.safe_load(f))
                return

    def deserialize(self, data):
        self.ID = data['id']
        self.Version = Version(data['id'])
        self.Name = data['name']
        self.Repository = data.get('repo-url')
        self.RepoLink = data.get('repo-link')
        self.Authors = []
        for adata in data.get('authors', []):
            author = Author()
            author.deserialize(adata)
            self.Authors.append(author)
        self.License = data.get('license')
        self.Requires = []
        for spec in data.get('requires',[]):
            if ' ' not in spec:
                self.Requires += [VersionTest(spec, None, None)]
            else:
                name, op, version = spec.split(' ')
                self.Requires += [VersionTest(name, op, version)]
        self.LoadAfter = data.get('load-after',[])
        self.Conflicts = data.get('conflicts',[])
        self.Patches = data.get('patches', [])
        self.Includes = data.get('includes', {})

    def CanLoad(self, modlist, loadedMods=None):
        for spec in self.Requires:
            if not spec.CheckAgainstMods(modlist, loadedMods):
                return False
        if loadedMods is not None:
            for modID in self.LoadAfter:
                if modlist.has(modID) and not modID in loadedMods:
                    return False
        return True

    def Update(self, repodir):
        if not os.path.isdir(repodir):
            print('Cloning...')
            pygit2.clone_repository(self.Repository, repodir)
        else:
            repo = pygit2.Repository(os.path.join(repodir, '.git'))
            print('Fetching with prune...')
            repo.remotes['origin'].fetch(pygit2.GIT_FETCH_PRUNE)
            repo.checkout('HEAD')

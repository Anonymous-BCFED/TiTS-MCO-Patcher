import yaml, os
from mco.mods.mod import Mod
class ModList(object):
    def __init__(self):
        self.AllMods=[]
        self.ModsByID={}

        self.allIncludes = {}

    def Load(self):
        self.AllMods=[]
        self.ModsByID={}
        mod_ids = []
        if os.path.isfile('mods.yml'):
            with open('mods.yml', 'r') as f:
                mod_ids = yaml.load(f, Loader=yaml.BaseLoader)
        for modID in mod_ids:
            self.loadMod(modID)

    def loadMod(self, modID):
        moddir = os.path.join('mods', modID)
        mod = Mod()
        mod.LoadFrom(moddir)
        self.AllMods += [mod]
        self.ModsByID[mod.ID]=mod
        self.allIncludes.update(mod.Includes)

    def save(self, modID):
        with open('mod.yml', 'w') as f:
            yaml.dump(self.AllMods, f, default_flow_style=False)

    def Apply(self):
        for mod in self.AllMods:
            mod.Apply()

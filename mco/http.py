from __future__ import print_function
from tqdm import tqdm
import shutil

import logging

log = logging.getLogger('mco.http')

import requests

HTTP_METHOD_GET = 'GET'
HTTP_METHOD_POST = 'POST'


def DownloadFile(url, filename, log_after=True, print_status=True, log_before=True, **kwargs):
    '''
    Download a file from url to filename.

    :param url:
        HTTP URL to download. (SSL/TLS will also work, assuming the cert isn't broken.)
    :param filename:
        Path of the file to download to.
    :param log_after:
        Produce a log statement after the download completes (includes URL).
    :param log_before:
        Produce a log statement before the download starts.
    :param print_status:
        Prints live-updated status of the download progress via tqdm. (May not work very well for piped or redirected output.)
    '''
    #kwargs['headers'] = dict(DEFAULT_HEADERS, **kwargs.get('headers', {}))
    r = None
    session = kwargs.get('session', requests)
    try:
        r = session.get(url, stream=True, **kwargs)
    except requests.exceptions.ConnectionError as e:
        log.exception(e)
        return False
    except UnicodeEncodeError as e:
        log.exception(e)
        return False

    if (r.status_code == 404):
        log.warn('404 - Not Found: %s', url)
        return False
    elif(r.status_code != 200):
        log.warn("Error code: %d", r.status_code)
        return False
    with open(filename+'.part', 'wb') as f:
        file_size = int(r.headers.get('Content-Length', '-1'))
        if file_size < 0:
            if 'Content-Length' not in r.headers:
                log.warn('Content-Length header was not received, expect progress bar weirdness.')
            else:
                log.warn('Content-Length header has invalid value: %r', r.headers['Content-Length'])
        if log_before:
            log.info("Downloading: %s Bytes: %s", filename, file_size if file_size > -1 else 'UNKNOWN')

        file_size_dl = 0
        block_sz = 8192

        progress = tqdm(total=file_size, unit_scale=True, unit_divisor=1024, unit='B', leave=False) if print_status else None
        for buf in r.iter_content(block_sz):
            if not buf or file_size == file_size_dl:
                break

            buf_len = len(buf)
            file_size_dl += buf_len
            f.write(buf)
            if print_status:
                #status = r"%10d/%10d  [%3.2f%%]" % (file_size_dl, file_size, file_size_dl * 100. / file_size)
                progress.update(buf_len)
                #status = status + chr(8) * (len(status) + 1)  - pre-2.6 method
                #print(status, end='\r')
        if progress is not None:
            progress.close()
    shutil.move(filename+'.part', filename)
    if log_after:
        log.info('Downloaded %s to %s (%dB)', url, filename, file_size_dl)

import argparse, os, sys, pygit2, shutil, logging, subprocess, yaml, platform, re

from zipfile import ZipFile

from lxml import etree
import stat

from buildtools import os_utils

from mco.constants import MCO_DIR, MCO_VERSION, MCO_PATCH_FORMAT_VERSION
from mco.mods.modlist import ModList
from mco.game.perk import Perk
from mco.game.custom_pc import CustomPC
from mco.input import getch
from mco.http import DownloadFile
from mco.as3.ast import dumpAST, parseAndPatch
from mco.as3.perk_registry_generator import loadDefinedPerks, genPerkRegistry, loadPerksFrom
from mco.as3.custom_pc_registry_generator import loadDefinedCustomPCs, loadCustomPCsFrom, genCustomPCRegistry
from mco.pygit2_callbacks import MCOProgressCallbacks

BAT='.bat' if platform.system() == 'Windows' else ''

TITS_GIT_REPO = 'https://github.com/OXOIndustries/TiTS-Public.git'

FLEXSDK_VERSION = '4.16.1'
FLEXSDK_URL=f'http://apache.osuosl.org/flex/{FLEXSDK_VERSION}/binaries/apache-flex-sdk-{FLEXSDK_VERSION}-bin.zip'

PLAYER_VERSION_MAJOR='31'
PLAYER_VERSION_MINOR='0'

# F:\Projects\TiTS-MCO-Patcher\TiTS-Modded\classes\TiTS.as(658): col: 6 Error: Syntax error: expecting identifier before if.
REG_COMPILE_ERROR = re.compile(rb'^(?P<file>[^\(]+)\((?P<line>\d+)\): col: \d+ Error: (.*)$')

JAVA_MAX_HEAP='2g'

LOG = None

def main():
    global LOG
    argp = argparse.ArgumentParser(prog='mco.sh' if sys.platform == 'linux' else 'mco.bat')
    subp = argp.add_subparsers()

    addp = subp.add_parser('add')
    addp.add_argument('mod_url', help='Git URL of the mod.')
    addp.add_argument('--branch', default='master', help='Branch of the git repository that should be checked out after clone.')
    addp.set_defaults(func=add_mod)

    rmp = subp.add_parser('remove', aliases=['rm'])
    rmp.add_argument('mod_id', help='ID of the mod being removed.')
    rmp.set_defaults(func=remove_mod)

    buildp = subp.add_parser('run')
    buildp.add_argument('--redo-flex', action="store_true", default=False, help="Redownload Flex dependencies.  Don't use this unless instructed to.")
    buildp.add_argument('--skip-patching', action="store_true", default=False, help="Skip cleaning+patching process.")
    buildp.set_defaults(func=run_command)

    buildp = subp.add_parser('clean')
    buildp.add_argument('--super-clean', action="store_true", default=False, help="Clears Flex and TiTS-Public, too.")
    buildp.set_defaults(func=clean_command)

    args = argp.parse_args()

    logger = logging.getLogger()
    handler = logging.StreamHandler()
    formatter = logging.Formatter(
            '%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
    handler.setFormatter(formatter)
    logger.handlers=[]
    logger.addHandler(handler)
    logger.setLevel(logging.INFO)
    logging.getLogger('patch').setLevel(logging.WARNING)
    LOG = logging.getLogger('mco')


    if hasattr(args,'func'):
        args.func(args)
    else:
        print('Error: no arguments specified.')
        argp.print_help()

def add_mod(args):
    prog = MCOProgressCallbacks()
    prog.startup()
    pygit2.clone_repository(args.mod_url, os.path.join(MCO_DIR, 'mods', args.mod_url.split('/')[-1].replace('.git', '')), checkout_branch=args.branch, callbacks=prog)
    prog.shutdown()

    mods = ModList()
    mods.Load()
    mods.loadMod(args.mod_url.split('/')[-1].replace('.git', ''))
    mods.save()

    return

def remove_mod(args):
    return

def clean_command(args):
    dirsToNuke=[
        os.path.join(MCO_DIR, 'bin'),
        os.path.join(MCO_DIR, 'TiTS-Modded'),
        os.path.join(MCO_DIR, 'COMPILE-ERROR.log'),
        os.path.join(MCO_DIR, 'data', 'found_perks.yml'),
    ]
    if args.super_clean:
        dirsToNuke += [
            os.path.join(MCO_DIR, 'lib', f'apache-flex-sdk-{FLEXSDK_VERSION}-bin'),
            os.path.join(MCO_DIR, 'lib', f'TiTS-Public'),
        ]
    for dirname in dirsToNuke:
        if os.path.isdir(dirname):
            LOG.info('Removing %s...', os.path.relpath(dirname, MCO_DIR))
            shutil.rmtree(dirname, ignore_errors=True)
        elif os.path.isfile(dirname):
            LOG.info('Removing %s...', os.path.relpath(dirname, MCO_DIR))
            os.remove(dirname)

# Sets up Adobe Flex with optional downloads, skipping prompts. User has agreed to licenses at this point.
def setup_flex(args):
    if not args.redo_flex and os.path.isfile(os.path.join(MCO_DIR, 'lib', f'apache-flex-sdk-{FLEXSDK_VERSION}-bin', 'lib', 'external', 'optional', 'flex-fontkit.jar')):
        LOG.info('Flex appears to be bootstrapped. Skipping bootstrap of Flex.')
        return

    FLEX_ZIP = os.path.join(MCO_DIR, f'apache-flex-sdk-{FLEXSDK_VERSION}.zip')
    FLEX_HOME = os.path.join(MCO_DIR, 'lib', f'apache-flex-sdk-{FLEXSDK_VERSION}-bin')

    if args.redo_flex:
        if os.path.isfile(FLEX_ZIP):
            LOG.info('Removing %s...', FLEX_ZIP)
            os.remove(FLEX_ZIP)
        if os.path.isdir(FLEX_HOME):
            LOG.info('Removing %s...', FLEX_HOME)
            shutil.rmtree(FLEX_HOME)

    LOG.info('Looking for %s...', FLEX_ZIP)
    if not os.path.isfile(FLEX_ZIP):
        LOG.info(f'Downloading Apache Flex SDK v{FLEXSDK_VERSION}...')
        DownloadFile(FLEXSDK_URL, FLEX_ZIP)

    if not os.path.isdir(FLEX_HOME):
        os.makedirs(FLEX_HOME)

    LOG.info('Extracting %s to %s...', FLEX_ZIP, FLEX_HOME)
    with ZipFile(FLEX_ZIP, 'r') as z:
        z.extractall(FLEX_HOME)

    buildprops=''
    with open(os.path.join(MCO_DIR, 'lib', f'apache-flex-sdk-{FLEXSDK_VERSION}-bin', 'build.properties'), 'r') as f:
        buildprops = f.read()
    buildprops = re.sub(r'-Xmx(?P<heap_end>\d+[kmg])', '-Xmx2g', buildprops)
    with open(os.path.join(MCO_DIR, 'lib', f'apache-flex-sdk-{FLEXSDK_VERSION}-bin', 'build.properties'), 'w') as f:
        f.write(buildprops)

    LOG.info('Setting up Apache Flex...')
    cwd = os.getcwd()
    os.chdir(os.path.join('lib', 'apache-flex-sdk-4.16.1-bin'))
    defs=[]
    packages=['font', 'air', 'flash', 'ofl', 'osmf', 'fontswf']
    for pkgID in packages:
        defs += [
            f'-D{pkgID}.donot.ask=true', f'-Ddo.{pkgID}.install=true'
        ]
    cmd = ['ant', 'frameworks-rsls']+defs
    LOG.info("Running %s...", ' '.join(cmd))
    subprocess.check_call(cmd)
    cmd=['ant', '-f', 'installer.xml']+defs
    LOG.info("Running %s...", ' '.join(cmd))
    subprocess.check_call(cmd)
    os.chdir(cwd)

def patch_as3proj(FLEX_HOME, infile, outfile):
    LOG.info('Patching %s -> %s...', infile, outfile)
    root = etree.parse(infile)
    for movie in root.xpath('//movie'):
        if 'version' in movie.attrib:
            movie.attrib['version']= PLAYER_VERSION_MAJOR
        if 'minorVersion' in movie.attrib:
            movie.attrib['minorVersion']= PLAYER_VERSION_MINOR

    found=False
    for match in root.xpath('//movie[@preferredSDK]'):
        LOG.info('  preferredSDK: %s -> %s',match.attrib['preferredSDK'],FLEX_HOME)
        match.attrib['preferredSDK']=FLEX_HOME
        found=True
    if not found:
        for match in root.xpath('//output'):
            LOG.info('  preferredSDK: Added as %s',FLEX_HOME)
            etree.SubElement(match, 'movie', {'preferredSDK': FLEX_HOME})
            break

    # No longer required!
    #etree.SubElement(root.find('libraryPaths'), 'element', {'path': 'flash.swc'})

    # Also probably not needed. Was considering it, but had issues since all the UIComponents were incompatible.
    #etree.SubElement(root.find('libraryPaths'), 'element', {'path': 'spark.swc'})

    # Change from Library\AS3\frameworks\FlashIDE to Library\AS3\frameworks\Flex4
    root.find('intrinsics')[0].set('path', 'Library\\AS3\\frameworks\\Flex4')
    with open(outfile, 'wb') as f:
        root.write(f, pretty_print=True)

#CS6_DIR=''
FLEX_HOME=''
FLASHDEVELOP_DIR=''
MODDED_DIR=''
TITS_REPO_DIR=''
def run_command(args):
    #global CS6_DIR, FLEX_HOME, FLASHDEVELOP_DIR, MODDED_DIR, TITS_REPO_DIR
    global FLEX_HOME, FLASHDEVELOP_DIR, MODDED_DIR, TITS_REPO_DIR
    import patch

    config = {}
    with open('config.yml', 'r') as f:
        config = yaml.load(f, Loader=yaml.BaseLoader)

    #print(repr(config))
    paths = config.get('paths',{})
    #CS6_DIR = paths.get('flash-cs6', 'C:\\Program Files (x86)\\Adobe\\Adobe Flash CS6')
    FLEX_HOME = os.path.join(MCO_DIR, 'lib', f'apache-flex-sdk-{FLEXSDK_VERSION}-bin')
    FLASHDEVELOP_DIR = paths.get('flashdevelop', 'C:\\Program Files (x86)\\FlashDevelop')

    # Check dependencies first.

    # Adobe Flash CS6
    #LOG.info('Looking for %s...', os.path.join(CS6_DIR, 'Common', 'Configuration', 'ActionScript 3.0'))
    #if not os.path.isdir(os.path.join(CS6_DIR, 'Common', 'Configuration', 'ActionScript 3.0')):
    #    LOG.error('You MUST install Flash CS6, and point paths.flash-cs6 in config.yml to the root directory (usually named `Adobe Flash CS6`).')
    #    LOG.error('TiTS requires the flash.* namespace because reasons, and it would require a major refactor to TiTS to change.')
    #    LOG.error('Obtaining Flash CS6 is an exercise left to you, as it is a commercial product we cannot include.')
    #    sys.exit(1)

    #LOG.info('Looking for %s...', os.path.join(FLASHDEVELOP_DIR, 'Tools'))
    #if not os.path.isdir(os.path.join(FLASHDEVELOP_DIR, 'Tools')):
    #    LOG.error('You did not bootstrap MCO.  Please follow the bootstrap procedure in README.')
    #    sys.exit(1)

    #if not os.path.isdir(os.path.join(FLEX_HOME, 'frameworks')):
    #    LOG.error('You did not bootstrap MCO.  Please follow the bootstrap procedure in README.')
    #    sys.exit(1)

    print(f'MCO v{MCO_VERSION}')
    print(f'####################################################################')
    print(f'###      IMPORTANT LEGAL NOTICE - DO NOT DISTRIBUTE *.SWF!       ###')
    print(f'####################################################################')
    print('The license for TiTS expressly forbids distribution of modified')
    print('binaries and source code. The entire purpose of MCO is to get around')
    print('this problem, so you don\'t need to distribute anything other than')
    print('small patches. Each build has just the mods you want and nothing')
    print('else, and other people might not like your choices.')
    print()
    print('YOU ALSO ACKNOWLEDGE YOU HAVE READ AND PREVIOUSLY AGREED TO THE')
    print('APACHE FLEX LICENSE, AND THOSE OF ITS OPTIONAL DEPENDENCIES.')
    print()
    print('DO NOT DISTRIBUTE THE COMPILED SWF, DO NOT DISTRIBUTE THE MODIFIED')
    print('CODE. IF YOU UNDERSTAND AND AGREE TO FOLLOW THOSE SIMPLE')
    print('INSTRUCTIONS, TYPE IN A LOWER-CASE H BELOW. DISTRIBUTION OF')
    print('COPYRIGHTED OXO INDUSTRIES BINARIES, SOURCE CODE, OR ART WILL INCUR')
    print('LEGAL LIABILITY TO YOU, AND YOU WILL BE BANNED FROM OUR SITES AND')
    print('FORUMS PERMANENTLY.')
    print()
    print('IF YOU DO NOT AGREE TO THESE TERMS, PRESS N BELOW.')
    print('TO AGREE, READ THE ENTIRE FUCKING DISCLAIMER SO YOU KNOW WHAT TO')
    print('AGREE TO.')
    print(f'####################################################################')
    print()
    inp = getch()
    # We get bytes on windows, for some reason. No idea of the encoding, hedging bets.
    if isinstance(inp, bytes):
        inp=inp.decode('utf-8')
    if inp != 'h':
        print('You disagreed with the ToS by typing '+repr(inp)+', terminating.')
        return

    # Finish bootstrapping apache-flex.
    setup_flex(args)
    LOG.info('Fixing jvm.config...')
    jvm_config_path = os.path.join(FLEX_HOME, 'bin', 'jvm.config')

    with open(jvm_config_path, 'w') as f:
        f.write('# Generated by MCO\n')
        f.write('# DO NOT MODIFY UNLESS YOU KNOW WHAT YOU\'RE DOING\n')
        #f.write('java.home={}\n'.format(new_javahome))
        f.write('java.args=-Xmx{} -Dsun.io.useCanonCaches=false\n'.format(JAVA_MAX_HEAP))
        f.write('env=\n')
        f.write('java.class.path=\n')
        f.write('java.library.path=\n')
    LOG.info('Generated %s', jvm_config_path)

    MODDED_DIR = modded_dir = os.path.join(MCO_DIR, 'TiTS-Modded')
    patch_repo = os.path.join(MCO_DIR, 'lib', 'TiTS-MCO-Patches')

    #if not args.skip_patching and os.path.isdir(modded_dir):
    #    LOG.info('Clearing old build...')
    #    shutil.rmtree(modded_dir)
    if os.path.isdir(modded_dir):
        try:
            pygit2.Repository(modded_dir)
        except pygit2.GitError as e:
            LOG.exception(e)
            LOG.info('Clearing old build...')
            def remove_readonly(func, path, _):
                "Clear the readonly bit and reattempt the removal"
                os.chmod(path, stat.S_IWRITE)
                func(path)
            shutil.rmtree(modded_dir, onerror=remove_readonly)


    LOG.info('Updating submodules...')
    repo = pygit2.Repository(MCO_DIR)
    # git submodule update --init
    prog = MCOProgressCallbacks()
    prog.startup()
    repo.update_submodules(init=True, callbacks=prog)
    prog.shutdown()

    TITS_REPO_DIR = os.path.join(MCO_DIR, 'lib', 'TiTS-Public')
    if not os.path.isdir(TITS_REPO_DIR):
        LOG.info('############################################################')
        LOG.info('#       Cloning TiTS Repo, this will be a while...         #')
        LOG.info('############################################################')
        prog = MCOProgressCallbacks()
        prog.startup()
        pygit2.clone_repository(TITS_GIT_REPO, TITS_REPO_DIR, callbacks=prog, bare=False)
        prog.shutdown()
    else:
        LOG.info('Fetching upstream updates...')
        tits_repo = pygit2.Repository(TITS_REPO_DIR)
        prog = MCOProgressCallbacks()
        prog.startup()
        tits_repo.remotes['origin'].fetch(callbacks=prog, prune=pygit2.GIT_FETCH_PRUNE)
        prog.shutdown()

    manifest = {}
    with open(os.path.join(patch_repo, 'config.yml'), 'r') as f:
        manifest = yaml.load(f, Loader=yaml.BaseLoader)
        if manifest['manifest-version'] != MCO_PATCH_FORMAT_VERSION:
            LOG.error('Patch manifest version is not synced.')
            LOG.error('Manifest version: '+repr(manifest['manifest-version']))
            return

    if not args.skip_patching:
        work_repo=None
        if not os.path.isdir(modded_dir):
            LOG.info('Copying repo for merged code (slow, will look frozen due to libgit bugs)...')
            #prog = MCOProgressCallbacks()
            #prog.startup()
            work_repo = pygit2.clone_repository(os.path.join(MCO_DIR, 'lib', 'TiTS-Public'), modded_dir)
            #prog.shutdown()
        else:
            work_repo = pygit2.Repository(modded_dir)

        LOG.info('Clearing new or ignored untracked files...')
        for filename, status in work_repo.status().items():
            UNTRACKED_NEW = pygit2.GIT_STATUS_WT_NEW
            if (status & (UNTRACKED_NEW|pygit2.GIT_STATUS_IGNORED)) != 0:
                LOG.info('rm %s',filename)
                if os.path.isfile(os.path.join(modded_dir, filename)):
                    os.remove(os.path.join(modded_dir, filename))
                if os.path.isdir(os.path.join(modded_dir, filename)):
                    def remove_readonly(func, path, _):
                        "Clear the readonly bit and reattempt the removal"
                        os.chmod(path, stat.S_IWRITE)
                        func(path)
                    shutil.rmtree(os.path.join(modded_dir, filename), onerror=remove_readonly)

        LOG.info('Resetting tree to commit %s...', manifest['tits']['public']['commit'])
        commit = work_repo.get(manifest['tits']['public']['commit'])
        work_repo.reset(manifest['tits']['public']['commit'], pygit2.GIT_RESET_HARD) # git reset --hard
        #work_repo.checkout_tree(commit)
        #work_repo.set_head(commit)

        os_utils.cmd([sys.executable, os.path.join(patch_repo, 'tools', 'unfuck.py'), '--use-report='+os.path.join(patch_repo, 'encoding.yml'), '--report-starts-at='+modded_dir, modded_dir], echo=True, critical=True)
        os_utils.cmd([sys.executable, os.path.join('scripts', 'checkPerks.py'), modded_dir], echo=True, show_output=True, critical=True)

        for patchname, patchdata in manifest['tits']['public']['patches'].items():
            patchdir = os.path.join(patch_repo, patchdata['dir'])
            LOG.info('Applying patch %r...', patchname)
            for patchfilename in patchdata['diffs']:
                p = patch.fromfile(os.path.join(patchdir, patchfilename))
                if not p:
                    LOG.error('FAILED TO PARSE.')
                    sys.exit(1)
                if not p.apply(strip=1, root=modded_dir):
                    LOG.error('FAILED TO PARSE.')
                    sys.exit(1)
            for filename in patchdata.get('new-files', []):
                LOG.info('Installing %s...', filename)
                srcfilename = os.path.join(patchdir, 'new_files', filename)
                destfilename = os.path.join(modded_dir, filename)
                if not os.path.isdir(os.path.dirname(destfilename)):
                    os.makedirs(os.path.dirname(destfilename), exist_ok=True)
                shutil.copy(srcfilename, destfilename)

        mods = ModList()
        mods.Load()
        if len(mods.AllMods) > 0:
            for mod in mods.AllMods:
                mod.Update()
            LOG.info('MCO now applying your mods...')
            mods.Apply()

        if not os.path.isdir(os.path.join(modded_dir, 'data')):
            os.makedirs(os.path.join(modded_dir, 'data'))

        INCLUDES_MAPFILE = os.path.join(modded_dir, 'data', 'includes.yml')
        with open(INCLUDES_MAPFILE, 'w') as f:
            yaml.dump(mods.allIncludes, f, default_flow_style=False)

        allPerks = loadDefinedPerks(config, mods)
        allCPCs = loadDefinedCustomPCs(config, mods)
        parseAndPatch(modded_dir, allPerks)

        # Generate perk registry.
        allKnownPerks={}
        for perkID, perkData in loadPerksFrom(os.path.join(modded_dir, 'data', 'found_perks.yml')).items():
            allKnownPerks[perkID] = Perk()
            allKnownPerks[perkID].deserialize(perkData)
            allKnownPerks[perkID].ID=perkID
            allKnownPerks[perkID].Save(modded_dir)
        for perkID, perkData in allPerks.items():
            allKnownPerks[perkID] = Perk()
            allKnownPerks[perkID].deserialize(perkData)
            allKnownPerks[perkID].ID=perkID
            allKnownPerks[perkID].Save(modded_dir)
        genPerkRegistry(list(allKnownPerks.values()), os.path.join(modded_dir, 'classes', 'GameData', 'PerkRegistry.as'))

        # Generate Custom PC registry.
        allKnownCPCs={}
        for cpcID, perkData in loadPerksFrom(os.path.join(modded_dir, 'data', 'found_custom_PCs.yml')).items():
            allKnownCPCs[cpcID] = CustomPC()
            allKnownCPCs[cpcID].deserialize(perkData)
            allKnownCPCs[cpcID].ID=cpcID
            allKnownCPCs[cpcID].Save(modded_dir)
        for cpcID, perkData in allCPCs.items():
            allKnownCPCs[cpcID] = CustomPC()
            allKnownCPCs[cpcID].deserialize(perkData)
            allKnownCPCs[cpcID].ID=cpcID
            allKnownCPCs[cpcID].Save(modded_dir)
        genCustomPCRegistry(list(allKnownCPCs.values()), os.path.join(modded_dir, 'classes', 'GameData', 'CustomPCRegistry.as'))

    patch_as3proj(FLEX_HOME,
        os.path.join(modded_dir, 'TiTSFD.as3proj'),
        os.path.join(modded_dir, 'TiTSFD-MCO.as3proj'))
    patch_as3proj(FLEX_HOME,
        os.path.join(modded_dir, 'TiTs-SE.as3proj'),
        os.path.join(modded_dir, 'TiTs-SE-MCO.as3proj'))

    #LOG.info('Installing Flash Pro CS6 namespaces... (flash.swc)')
    #shutil.copy(os.path.join(CS6_DIR, 'Common', 'Configuration', 'ActionScript 3.0', 'libs', 'flash.swc'), os.path.join(modded_dir, 'flash.swc'))

    LOG.info('Installing new ant build.xml...')
    shutil.copy(os.path.join(MCO_DIR, 'templates', 'build.xml'), os.path.join(modded_dir, 'build.xml'))

    compile_swf('release', os.path.join(MODDED_DIR, 'bin', 'TiTS-MCO.swf'), os.path.join('bin', 'TiTS-MCO.swf'))
    compile_swf('release-editor', os.path.join(MODDED_DIR, 'bin', 'TiTS-SE-MCO.swf'), os.path.join('bin', 'TiTS-SE-MCO.swf'))

    LOG.info('********************************')
    LOG.info('* COMPILE SUCCESSFUL! Check bin/.')
    LOG.info('********************************')

def compile_swf(target: str, intermediate_file: str, output_file: str) -> None:
    LOG.info('Compiling %s...', output_file)
    run_ant(target)
    if not os.path.isdir('bin'):
        os.makedirs('bin')
    shutil.copy(intermediate_file, output_file)

def run_ant(target: str) -> None:
    LOG.info('Running `ant %s` with FLEX_HOME=%s', target, FLEX_HOME)
    os.environ['FLEX_HOME'] = FLEX_HOME
    #os.environ['CS6_DIR'] = CS6_DIR
    cwd = os.getcwd()
    os.chdir(MODDED_DIR)
    # Yes, I tried building with just MXMLC.  It imploded spectacularly.
    try:
        subprocess.run([
            'ant',
            target
        ], check=True)
    except subprocess.CalledProcessError as cpe:
        os.chdir(cwd)
        if cpe.stdout is not None:
            files=set()
            for line in cpe.stderr.splitlines():
                m = REG_COMPILE_ERROR.match(line)
                if m is not None:
                    files.add(m.group('file'))

            for filename in files:
                filenamedest = filename.decode('utf-8')
                filenamesrc = os.path.join(TITS_REPO_DIR, os.path.relpath(filenamedest, start=MODDED_DIR))
                if os.path.isfile(filenamesrc):
                    dumpAST(filenamesrc, filenamedest+'.ast.xml')
                    dumpAST(filenamesrc, filenamedest+'.orig-ast.xml')
                    shutil.copy(filenamesrc, filenamedest[0:-3]+'.orig.as')
                else:
                    LOG.warning("Skipping %s (generated)", filenamesrc)

            for line in (cpe.stdout+cpe.stderr).splitlines():
                LOG.critical(line.decode('utf-8'))

            with open('COMPILE-ERROR.log', 'wb') as f:
                f.write(cpe.stdout+cpe.stderr)
            LOG.critical('AST dumped for each errored file in the form of {filename}.xml. This will help with debugging the patcher.')
            LOG.critical('Please yell at the MCO devs, then update and re-run when fixed.')
            sys.exit(1)
    os.chdir(cwd)

if __name__ == '__main__':
    main()

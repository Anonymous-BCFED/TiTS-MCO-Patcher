#!/bin/bash
set -ex
PYTHON="$PYTHON"
[ -z "$PYTHON" ] && [ ! -z "${VIRTUAL_ENV}" ] && { PYTHON=`command -v python`; }
[ -z "$PYTHON" ] && command -v python3.8 && { PYTHON=`command -v python3.8`; }
[ -z "$PYTHON" ] && command -v python3.7 && { PYTHON=`command -v python3.7`; }
[ -z "$PYTHON" ] && command -v python3.6 && { PYTHON=`command -v python3.6`; }
[ -z "$PYTHON" ] && command -v python3 && { PYTHON=`command -v python3`; echo "WARNING: Using fallback interpreter!"; }
if [ -z "$PYTHON" ]; then
  echo "Unable to find an available python3 interpreter. Please install python >=3.6."
  exit 1
fi
echo \$PYTHON=$PYTHON

$PYTHON -m mco $*

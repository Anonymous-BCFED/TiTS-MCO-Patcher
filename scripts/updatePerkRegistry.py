import codecs
import os
import shutil

import yaml
import chardet
import ftfy
from mco.log_utils import log
from mco.as3.encoding import detect_encoding, unfuckFile

ORIGBASEDIR = 'lib/TiTS-Public/classes/GameData/PerkClasses'
DESTBASEDIR = 'TiTS-Modded/classes/GameData/PerkClasses'


def getCurrentIndent(line):
    indent = ''
    for c in line:
        if c not in (' ', '\t'):
            return indent
        indent += c


def cleanName(name):
    newName = ''
    for c in name:
        c = c.upper()
        if c in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ ':
            newName += c
    return newName.replace(' ', '_')

def main():
    log.info("UPDATEPERKREGISTRY 1.0")

    classnames = []
    classname2Realname = {}
    with open('data/merged_perks.yml' , 'r') as f:
        data = yaml.load(f)
        for perk in data.values():
            className = perk['className']
            perkName = perk['perkName']
            classnames += [className]
            classname2Realname[className]=perkName
            log.info('Perk Name: %s', perkName)
    with codecs.open('data/ALLPERKS.yml', 'w', encoding='utf-8-sig') as f:
        yaml.dump({'Perks': classname2Realname}, f)
    seclassnames=[]
    seclassname2Realname={}
    with open('data/merged_status_effects.yml' , 'r') as f:
        data = yaml.load(f)
        for se in data.values():
            className = se['className']
            statusEffectName = se['statusName']
            seclassnames += [className]
            seclassname2Realname[className]=statusEffectName
            log.info('Status Effect Name: %s', statusEffectName)
    with codecs.open('data/ALLSTATUSEFFECTS.yml', 'w', encoding='utf-8-sig') as f:
        yaml.dump({'StatusEffects': seclassname2Realname}, f)
    

if __name__ == '__main__':
    main()

package classes.GameData.PerkClasses {
	import classes.*;
	import classes.Creature;
	import classes.GLOBAL;
	import classes.GameData.PerkData;
	import classes.StorageClass;
	import classes.kGAMECLASS;
	import classes.GameData.*;

	/**
	 * Test perk
	 * @author Anonymous-BCFED
	 */
	public class MyExamplePerk extends PerkData {
		public function NukiSyndrome() {
			setID("my-example-perk");
			perkName = "Example Perk";
			setAllNames(["my-example-perk"]);
			perkDescription = "Allows you to explode your enemies to death";
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Attach to events here
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Detach from events here
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:MyExamplePerk = new MyExamplePerk();
			return _clone(sc, pd);
		}

	}
}

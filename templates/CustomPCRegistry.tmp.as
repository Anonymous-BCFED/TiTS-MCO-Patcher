package classes.GameData {
	import classes.CustomPCs.*;
	/**
	 * All known custom PCs.
	 * @generated
	 * @author Anonymous-BCFED
	 * @note Template is templates/CustomPCRegistry.tmp.as
	 */
	public class CustomPCRegistry {
		private static var _all:Array;
		public static function get all():Array {
			if (CustomPCRegistry._all == null) CustomPCRegistry.Setup();
			return CustomPCRegistry._all;
		}
		/// BEGIN CUSTOM PC STATICS
		/// END CUSTOM PC STATICS
		public static function Setup():void {
			_all = new Array();
			/// BEGIN CUSTOM PC INSTANTIATION
			/// END CUSTOM PC INSTANTIATION
		}

		public static function getByName(name:String):BaseCustomPC {
			return CustomPCRegistry.all[name];
		}
		public static function check(name:String):BaseCustomPC {
			var nameNormed:String = name.toLowerCase();
			for (var cpcKey:String in all) {
				var cpc:BaseCustomPC = (_all[cpcKey] as BaseCustomPC);
				if (cpc.matches(nameNormed)) {
					return cpc;
				}
			}
			return null;
		}
	}
}

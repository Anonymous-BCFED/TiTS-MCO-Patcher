package classes.GameData
{
	import classes.GameData.PerkClasses.*;
	import classes.StorageClass;
	/**
	 * All known perks.
   * @generated
	 */
	public class PerkRegistry
	{
		public static function get all():Array {
			if (PerkRegistry._all == null) PerkRegistry.Setup();
			return PerkRegistry._all;
		}
		private static var _all:Array;
		public static var UNKNOWN:PerkData = new PerkData();
		/// BEGIN PERK STATICS
		/// END PERK STATICS
		public static function Setup():void
		{
			_all = new Array();
			_all['UNKNOWN'] = UNKNOWN;
			/// BEGIN PERK INSTANTIATION
			/// END PERK INSTANTIATION
		}

		public static function getByName(name:String):PerkData {
			return PerkRegistry.all[name];
		}

		public static function forEach(perks:Array, callback:Function):void {
			for each (var sc:StorageClass in perks) {
				var perk:PerkData = (PerkRegistry.all[sc.storageName] as PerkData);
				callback.call(perk.clone(sc))
			}
		}
	}
}

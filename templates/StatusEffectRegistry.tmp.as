package classes.GameData
{
	import classes.GameData.StatusEffects.*;
	import classes.StorageClass;
	/**
	 * All known perks.
   * @generated
	 */
	public class StatusEffectRegistry
	{
		public static function get all():Array {
			if (StatusEffectRegistry._all == null) StatusEffectRegistry.Setup();
			return StatusEffectRegistry._all;
		}
		private static var _all:Array;
		public static var UNKNOWN:StatusEffect = new StatusEffect();
		/// BEGIN STATUS STATICS
		/// END STATUS STATICS
		public static function Setup():void
		{
			_all = new Array();
			_all['UNKNOWN'] = UNKNOWN;
			/// BEGIN STATUS INSTANTIATION
			/// END STATUS INSTANTIATION
		}

		public static function getByName(name:String):StatusEffect {
			return StatusEffectRegistry.all[name];
		}

		public static function forEach(statuses:Array, callback:Function):void {
			for each (var sc:StorageClass in statuses) {
				var status:StatusEffect = (StatusEffectRegistry.all[sc.storageName] as StatusEffect);
				callback.call(status.clone(sc))
			}
		}
	}
}

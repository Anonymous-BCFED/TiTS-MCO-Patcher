# TiTS: MCO - Patcher

![Ready](https://img.shields.io/badge/Ready-NO-red.svg)
![Discord](https://img.shields.io/discord/567101908330872832.svg)

[Trials in Tainted Space](https://www.fenoxo.com/play-games/) is a Flash text game by OXO Industries.

*TiTS: Modular Code Overhaul* ("TiTS:MCO") is a toolkit that makes modding legal and possible.

This is the patcher that downloads the original code, patches it for you, and then compiles it into a game for your personal enjoyment.

## Why is this a thing?
TiTS is a fun game, but it has a terrible license with regard to modding. In short, you can mod the game for personal use, but you cannot distribute the changed code UNLESS
you ask Fenoxo for written permission, and even then, the license can be yanked at any time.  This makes no sense legally, and makes a robust modding community nearly
impossible with the current license.

TiTS: MCO, as a whole, actually boils down into two projects:

* The mod itself, which makes modding the code easier via events and inheritable objects and registries
* A patcher that downloads the original game code, patches it with one or more patchsets, and compiles it for you.

The patcher exploits a loophole in the license, which states that distributing the modified code is forbidden, but does not forbid distribution of patches.

## What does MCO do?
In short, MCO downloads, patches, and compiles the game and the save editor for you, all to get around Fen's weird legal prohibitions on modding.

### Technical version
1. MCO downloads and installs all of the dependencies needed to compile and patch TiTS with MCO's patchsets. This includes runtimes like Python and Java, tools like Apache Flex, and the patchsets themselves.
1. MCO uses pygit2 to retrieve TiTS itself and checkout the exact commit the *patchset* is compatible with.
1. MCO then uses the patch library to load and apply unified diffs to the stock code, and adds new files. It does the same with mods.
1. The TiTS-MCO-Transform system is called, which performs thousands of automated modifications to the code based on AST signature matches.
  * Perk Overhaul: `creature.hasPerk("Perk ID")` becomes `PerkRegistry.PERK_CONST.hasPerk(creature)`, `creature.createPerk("Perk ID", ...)` becomes `PerkRegistry.PERK_CONST.applyTo(creature, ...)`
    * The purpose of this is to make it harder to fatfinger PerkIDs, and make it easier to list them all in the fixed save editor.
  * Injection of new `include "filename";` directives for mods based on rules from `mod/info.yml`, reducing patch conflicts.
  * Extraction of some information from the code for use in mco's mods.
1. MCO then generates `PerkRegistry` and `CustomPCRegistry` with the combined stock and mod objects.
1. MCO prepares the build environments for FlashDevelop, and ant.
1. MCO compiles the game itself with `ant release`, and then the save editor with `ant release-editor`.

## Legal
TiTS is a product of OXO Industries. The MCO Team is not affiliated with OXO Industries in any way, shape, or form.  Characters, places, and events are property of their respective
owners. MCO makes no claim to any copyrighted material.

TiTS: MCO is not endorsed, condoned, nor supported by OXO Industries, and is not a product of OXO Industries.

Because of licensing restrictions, **you may not distribute the output binary `*.swf` files, nor the modified code.** Sorry, but
[that's not under our control](https://github.com/OXOIndustries/TiTS-Public/blob/b7f7ada521a6f38ce07cecd55ad9d690dc1be1f2/LICENSE.md).  Please petition Fenoxo for a more permissive code license.

## Installing

### Preflight
* You need to install [git](https://git-scm.org) and [git-lfs](https://git-lfs.github.com/) for your platform.
* You need at least some experience with the terminal/shell in your OS.
  * A GUI will be coming eventually.
* Currently supported bootstrappable platforms:
  * Ubuntu 18.04
  * Windows 10

### Downloading
Run `git clone --recursive https://gitlab.com/Anonymous-BCFED/TiTS-MCO-Patcher.git TiTS-MCO` to download the patcher to `TiTS-MCO`. **Do not download as a ZIP!** You will break things if the git repo isn't present.

### Bootstrap
Bootstrapping is the process of preparing your environment for the MCO patcher.

#### Windows
Windows requires [chocolatey](https://chocolatey.org/) to be preinstalled.  MCO uses chocolatey to install prerequisites like git-lfs, Java JDK, etc.

* Windows 10 - `call ./bootstrap/windows/10.bat` ***(AS ADMINISTRATOR, DO NOT RUN FROM EXPLORER)***

#### Linux
* (Ku|Xu|U)buntu 18.04 - `./bootstrap/linux/ubuntu/18.04.sh`. You will be asked to enter your `sudo` password. Tested on Kubuntu 18.04.2.

## Building the game
MCO is a framework, so you need to tell it to install which mods, and then to patch the game.

**This will take a long time**, since:
* MCO will download the [TiTS source code](https://github.com/OXOIndustries/TiTS-Public) for you, and it's at least a GB in size due to poor binary asset management. (Fen, pls use git-lfs like we do.)
* Thousands of files of source code are standardized to UTF-8 from ASCII, CP1252, and whatever other encodings they currently have. Line endings are also standardized.
* All of that source code is then *rewritten* by TiTS-MCO-Transform, which does a bunch of highly complex AST analysis and modification. Again, takes time.
* Apache Flex (the compile toolchain) and Adobe AIR are both installed and configured to have increased stack heap. AIR and Flex are both large and have tons of dependencies.
* The game itself has lots of code and images to embed, which takes time to compile.

### Specifying mods
To add a mod, link to its git repo:

`./mco.bat add https://gitlab.com/Anonymous-BCFED/TiTS-MCO-Test.git`

MCO will then clone and install that mod for you.

### Building the game
`./mco.bat run`

The game will be compiled to `bin/`. Again, **DO NOT DISTRIBUTE THE COMPILED FILES.**

## Making mods
The modding framework is pretty close to completion, but still in flux. It will be documented soon.

## Development Team

| Name | Role | Contact Info
|---:|:---:|---|
| Anonymous-BCFED | Lead Developer | Discord: Anonymous-BCFED#8233 |
| VentureGuy | AST Magician | Discord: VentureGuy#4114, Email: friedfrogs888@yahoo.com |

## Support/Contact

* **IRC:** TODO
* **Discord:** https://discord.gg/6qj7RFK
